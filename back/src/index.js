// back/src/index.js
import app from './app'
import initializeDatabase from './db'
import {  isLoggedIn } from './auth'
import path from 'path'
import multer from 'multer' 


const multerStorage = multer.diskStorage({
  destination: path.join( __dirname, '../public/images'),
  filename: (req, file, cb) => {
    const { fieldname, originalname } = file
    const date = Date.now()
    // filename will be: image-1345923023436343-filename.png
    const filename = `${fieldname}-${date}-${originalname}` 
    cb(null, filename)
  }
})
const upload = multer({ storage: multerStorage  })



const start = async () => {
  const controller = await initializeDatabase()

  
  
  app.get('/mypage', isLoggedIn, async ( req, res, next ) => {
    try{
      const { order, desc } = req.query;
      const { sub, name} = req.user
      const user = await controller.createUserIfNotExists({sub, name})
      const cups = await controller.getAllCups({order, desc, auth0_sub:sub})
      user.cups = cups
      res.json({ success: true, result: user });
    }catch(e){
      next(e)
    }
  })

  app.post('/upload-resume', upload.single('resume'), (req, res, next)=>{
    console.log(req.file)
  })
  
  app.post('/galleries-photos', upload.array('photos',10), (req, res, next)=>{
    console.log(req.files)
  })
  
  app.get('/', (req, res, next) => res.send("ok"));

  // CREATE
  app.get('/cups/add', isLoggedIn, async (req, res, next) => {
    const { type, country,water} = req.query
    const image = req.file && req.file.filename

    const auth0_sub = req.user.sub
    const result = await controller.addCup({type,country,water,image,auth0_sub})
    res.json({success:true, result})
  })

  // READ
  app.get('/cups/get/:id', async (req, res, next) => {
    const { id } = req.params
    const Flower = await controller.getCup(id)
    res.json({success:true, result:Flower})
  })

  // DELETE
  app.get('/cups/delete/:id', isLoggedIn, async (req, res, next) => {
    const auth0_sub = req.user.sub

    const { id } = req.params
    const result = await controller.deleteCup({id,auth0_sub})
    res.json({success:true, result})
  })

  // UPDATE
  app.get('/cups/update/:id', isLoggedIn, async (req, res, next) => {

    const auth0_sub = req.user.sub
    const { id } = req.params
    const image = req.file && req.file.filename
    const { type, country,water } = req.query
    const result = await controller.updateCup(id,{type,country,water,image,auth0_sub})
    res.json({success:true, result})
  })

  // LIST
  app.get('/cups/list', async (req, res, next) => {
    const { order, desc } = req.query;
    const flowers = await controller.getAllCups({order, desc})
    res.json(flowers)
  })



  app.listen(8080, () => console.log('server listening on port 8080'))
}

start()

