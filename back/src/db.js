// back/src/db.js
import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const joinSQLStatementKeys = (keys, values, delimiter , keyValueSeparator='=') => {
  return keys
    .map(propName => {
      const value = values[propName];
      if (value !== null && typeof value !== "undefined") {
        return SQL``.append(propName).append(keyValueSeparator).append(SQL`${value}`);
      }
      return false;
    })
    .filter(Boolean)
    .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {

  const nowForSQLite = () => new Date().toISOString().replace('T',' ').replace('Z','')
  const db = await sqlite.open('./db.sqlite');

  

  const addCup= async (props) => {

    const { type, country, water,image ,auth0_sub} = props
    const date = nowForSQLite();

    try{
      const result = await db.run(SQL`INSERT INTO cups (type,country,water,image, date, auth0_sub) VALUES (${type}, ${country},${water}, ${date},${image} ${auth0_sub})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  
  const deleteCup= async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM cups WHERE cup_id = ${id} AND auth0_sub = ${auth0_sub}`);
      if(result.stmt.changes === 0){
        throw new Error(`Cup"${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the Cup"${id}": `+e.message)
    }
  }
  
  const updateCup= async (id, props) => {

    try {
      const previousProps = await getCup(cup_id)
      const newProps = {...previousProps, ...props }
      const statement = SQL`UPDATE cups SET `
        .append(
          joinSQLStatementKeys(
            ["type", "country","water", "image"],
            newProps,
            ", "
          )
        )
        .append(SQL` WHERE `)
        .append(
          joinSQLStatementKeys(
            ["cup_id", "auth0_sub"],
            { cup_id:cup_id, auth0_sub:props.auth0_sub },
            " AND "
          )
        );


      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the Cup${id}: ` + e.message);
    }
  }



  const getCup= async (id) => {
    try{
      const cupList = await db.all(SQL`SELECT cup_id AS id, type,country,water, image FROM cups WHERE cup_id = ${id}`);
      const cup = cupList[0]
      return cup
    }catch(e){
      throw new Error(`couldn't get the cup${id}: `+e.message)
    }
  }
  
  const getAllCups = async props => {
    const { orderBy, auth0_sub, desc, start } = props

    try {
      const statement = SQL`SELECT cup_id AS id, type, country,water,image, date, auth0_sub FROM cups`;
      if(auth0_sub){
        statement.append(SQL` WHERE auth0_sub = ${auth0_sub}`)
      }
      const isValidOrderBy = /name|email|date/.test(orderBy)
      if(isValidOrderBy){
        statement.append( desc? SQL` ORDER BY ${orderBy} DESC` : SQL` ORDER BY ${orderBy} ASC`);
      }
      const rows = await db.all(statement);

      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve Cups: ` + e.message);
    }
  };

  const createUserIfNotExists = async props => {
    const { sub, name } = props;
    const answer = await db.get(
      SQL`SELECT auth0_sub FROM users WHERE auth0_sub = ${sub}`
    );
    if (!answer) {
      await createUser(props)
      return {...props, firstTime:true } // if the user didn't exist, make that clear somehow
    }
    return props;
  };

  const createUser = async props => {
    const { sub, name } = props;
    const result = await db.run(SQL`INSERT INTO users (auth0_sub, usernick) VALUES (${sub},${name});`);
    return result.stmt.lastID;
  }

  
  
  const controller = {
    addCup,
    deleteCup,
    updateCup,
    getCup,
    getAllCups,
    createUserIfNotExists,
    createUser

  }

  return controller
}


export default initializeDatabase


