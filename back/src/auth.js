import jwt from 'express-jwt'
import jwksRsa from 'jwks-rsa'

/**
 * this is our users database
 **/

const AUTH0_DOMAIN = 'square661.au.auth0.com'
const AUTH0_CLIENT_ID = 'QkVOrjz6hHDDOgqPjon7rI5bu1ujcr6A'



export const isLoggedIn = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${AUTH0_DOMAIN}/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: AUTH0_CLIENT_ID,
  issuer: `https://${AUTH0_DOMAIN}/`,
  algorithms: ['RS256']
});

