import React from 'react'
import * as auth0Client from "./auth"


export default class Cup extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  renderViewMode() {
    const { id, type,country,date,water,image, auth0_sub, deleteCup } = this.props;
    const isLoggedIn = auth0Client.isAuthenticated();
    const current_logged_in_user_id = isLoggedIn && auth0Client.getProfile().sub
    const id_user = auth0_sub === current_logged_in_user_id
    console.log(auth0_sub, current_logged_in_user_id)
    return (
      <div>
        <span>
        { image && <img src={`//localhost:8080/images/${image}`} alt={`the avatar of ${type}`}/> }
          {id} - {type} - {country} - {water} - {date}
        </span>
        { id_user && isLoggedIn ?
          <div>
              <button onClick={this.toggleEditMode} className="success">
                edit
              </button>
              <button onClick={() => deleteCup(id)} className="warning">
                x
              </button>
          </div>
        : false
        }
      </div>
    );
  }
  renderEditMode() {
    const { type, country,water } = this.props;
    
    return (
      <form
        classtype="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="type"
          name="Cup_type_input"
          defaultValue={type}
        />
        <input
          type="text"
          placeholder="country"
          name="Cup_country_input"
          defaultValue={country}
        />
          <input
          type="text"
          placeholder="water"
          name="Cup_water_input"
          defaultValue={water}
        />
                <input
          type="file"
          name="cup_image_input"
        />

        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" classtype="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    // stop the page from refreshing
    evt.preventDefault();
    // target the form
    const contact_image_input = form.contact_image_input;
    const form = evt.target;
    // extract the two inputs from the form
    const Cup_type_input = form.Cup_type_input;
    const Cup_country_input = form.Cup_country_input;
    const Cup_water_input = form.Cup_water_input;

    // extract the values
    const type = Cup_type_input.value;
    const country = Cup_country_input.value;
    const water = Cup_water_input.value;
    const image = contact_image_input.files[0];


    // get the id and the update function from the props
    const { id, updateCup } = this.props;
    // run the update Cup function
    updateCup(id, { type, country,water,image });
    // toggle back view mode
    this.toggleEditMode();
  };
  
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}
