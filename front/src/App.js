import React, { Component } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Cup from "./Cup";
import { Transition } from 'react-spring'
import { pause, makeRequestUrl } from "./utils.js";
import "./App.css";
import * as auth0Client from "./auth";
import IfAuthenticated from './IfAuthenticated'
import SecuredRoute from './SecuredRoute'
import CupList from './ CupList'


let body = null

const makeUrl = (path, params) => makeRequestUrl(`http://localhost:8080/${path}`, params)

class App extends Component {
  state = {
    cups_list: [],
    error_message: "",
    type: "",
    country: null,
    water: "",
    isLoading: false,
    checkingSession: true

  };



  isLogging = false;
  login = async () => {
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try {
      await auth0Client.handleAuthentication();
      await this.getPersonalPageData(); // get the data from our server
      this.props.history.push('/profile')
    } catch (err) {
      this.isLogging = false
      toast.error(err.message);
    }
  }
  handleAuthentication = ({ history }) => {
    this.login(history)
    return <p>wait...</p>
  }



  getCup = async id => {
    // check if we already have the cup
    const previous_cup = this.state.cups_list.find(
      cup => cup.id === id
    );
    if (previous_cup) {
      return; // do nothing, no need to reload a cup we already have
    }
    try {
      const url = makeUrl(`cups/get/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of cups
        const cup = answer.result;
        const cups_list = [...this.state.cups_list, cup];
        this.setState({ cups_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteCup = async id => {
    try {
      const url = makeUrl(`cup/delete/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const cups_list = this.state.cups_list.filter(
          cup => cup.id !== id
        );
        this.setState({ cups_list });
        toast("cup deleted")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateCup = async (id, props) => {
    try {

      const url = makeUrl(`cup/update/${id}`, {
        type: props.type,
        country: props.country,
        water: props.water
      });

      if (props.image) {
        body = new FormData();
        body.append(`image`, props.image)
      }

      const response = await fetch(url, {
        method: 'POST',
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });

      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const cups_list = this.state.cups_list.map(cup => {
          // if this is the cup we need to change, update it. This will apply to exactly
          // one cup2
          if (cup.id === id) {
            const new_cup = {
              id: cup.id,
              type: props.type || cup.type,
              country: props.country || cup.country,
              water: props.water || cup.water
            };
            toast("cup updated")
            return new_cup;

          }
          // otherwise, don't change the cup at all
          else {
            return cup;

          }
        });
        this.setState({ cups_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createCup = async props => {
    try {

      const { type, country, water, image } = props;
      const url = makeUrl(`cups/add`, {
        type,
        country,
        water
      });
      if (image) {
        body = new FormData();
        body.append(`image`, image)
      }

      const response = await fetch(url, {
        method: 'POST',
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });

      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const cup = { type, country, water, id };
        const cups_list = [...this.state.cups_list, cup];
        this.setState({ cups_list });
        toast("cup added")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getCupsList = async order => {
    try {
      this.setState({ isLoading: true });
      const url = makeUrl(`cups/list`, { order, token: this.state.token })
      const response = await fetch(url);

      await pause()
      const answer = await response.json();
      if (answer) {
        const cups_list = answer;
        this.setState({ cups_list, isLoading: false });
        toast("cup loaded")

      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  async componentDidMount() {
    if (this.props.location.pathname === '/callback') {
      this.setState({ checkingSession: false });
      return;
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
    } catch (err) {
      if (err.error !== 'login_required') {
        console.log(err.error);
      }
      this.setState({ checkingSession: false });
    }
    this.getCupsList();
  }




  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    const image = evt.target.cup_image_input.files[0]
    // extract type and country from state
    const { type, country, water } = this.state;
    // create the cup from mail and country
    this.createCup({ type, country, water, image });
    // empty type and country so the text input fields are reset
    this.setState({ type: "", country: "", water: "" });
  };
  renderHomePage = () => {
    const { cups_list, error_message, isLoading } = this.state;
    return (
      <div classtype="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}

        {isLoading ? (
          <p>loading...</p>
        ) : (
            <Transition
              items={cups_list}
              keys={cup => cup.id}
              from={{ transform: "translate3d(-100px,0,0)" }}
              enter={{ transform: "translate3d(0,0px,0)" }}
              leave={{ transform: "translate3d(-100px,0,0)" }}
            >
              {cup => style => (
                <div style={style}>
                  <Cup
                    key={cup.id}
                    id={cup.id}
                    type={cup.type}
                    country={cup.country}
                    water={cup.water}
                    date={cup.date}
                    auth0_sub={cup.auth0_sub}
                    updateCup={this.updateCup}
                    deleteCup={this.deleteCup}
                  />
                </div>
              )}
            </Transition>
          )}


        <ToastContainer />

      </div>
    );
  }
  onLoginSubmit = (evt) => {
    evt.preventDefault();
    const username = evt.target.username.value
    const password = evt.target.password.value
    if (!username) {
      toast.error("username can't be empty");
      return
    }
    if (!password) {
      toast.error("password can't be empty");
      return
    }
    this.login(username, password)
  }

  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated()
    if (isLoggedIn) {
      // user is logged inuser
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return (
      <button onClick={auth0Client.signIn}>Sign In</button>
    );
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;
    const user_cups = this.state.user.cups.map(cupFromUser =>
      this.state.cups_list.find(
        cupFromMain => cupFromMain.id === cupFromUser.id
      )
    );
    return (
      <div>
        Hello, {nick}!{" "}
        <button
          onClick={() => {
            auth0Client.signOut();
            this.setState({});
          }}
        >
          logout
        </button>
        <div>
          {/* <CupList cups_list={user_cups} /> */}
        </div>
      </div>
    );
  }

  renderCupPage = ({ match }) => {
    const id = match.params.id;
    // find the cup:
    // eslint-disable-next-line eqeqeq
    const cup = this.state.cups_list.find(cup => cup.id == id);
    // we use double equality because our ids are numbers, and the id provided by the router is a string
    if (!cup) {
      return <div>{id} not found</div>;
    }
    return (
      <Cup
        id={cup.id}
        name={cup.name}
        country={cup.country}
        water={cup.water}
        image={cup.image}
        auth0_sub={cup.auth0_sub}
        updateCup={this.Updatecup}
        deleteCup={this.Deletecup}
      />
    );
  };


  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, { headers: { 'Authorization': `Bearer ${auth0Client.getIdToken()}` } });
      const answer = await response.json();
      if (answer.success) {
        const user = answer.result;
        console.log(user)
        this.setState({ user })
        if (user.firstTime) {
          toast(`welcome ${user.usernick}! We hope you'll like it here'`);
        }
        toast(`hello ${user.usernick}'`);
      }
      else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  }

  renderProfilePage = () => {
    if (this.state.checkingSession) {
      return <p>validating session...</p>
    } else {
      return (
        <div>
          <p>profile page</p>
          {this.renderUser()}
        </div>
      );
    }
  };


  renderCreateForm = () => {

    return (
      <form classtype="third" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="type"
          onChange={evt => this.setState({ type: evt.target.value })}
          value={this.state.type}
        />
        <input
          type="text"
          placeholder="country"
          onChange={evt => this.setState({ country: evt.target.value })}
          value={this.state.country}
        />
        <input
          type="text"
          placeholder="water"
          onChange={evt => this.setState({ water: evt.target.value })}
          value={this.state.water}
        />
        <input
          type="file"
          name="cup_image_input"
        />

        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" classtype="button" />
        </div>
      </form>
    )
  }



  renderContent() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }
    return (
      <Switch>
        <Route path="/" exact render={this.renderHomePage} />
        <Route path="/cup/:id" render={this.rendercupPage} />
        <Route path="/profile" render={this.renderProfilePage} />
        <SecuredRoute path="/create" render={this.renderCreateForm} checkingSession={this.state.checkingSession} />
        <Route path="/callback" render={this.handleAuthentication} />
        <Route render={() => <div>not found!</div>} />
      </Switch>
    )
  }



  render() {
    return (
      <div classtype="App">
        <div>
          <Link to="/">Home</Link> |<Link to="/profile">profile</Link> |
          <IfAuthenticated>
            <Link to="/create">create</Link>
          </IfAuthenticated>
        </div>

        {this.renderContent()}
        <ToastContainer />
      </div>)

  }
}

export default withRouter(App)

