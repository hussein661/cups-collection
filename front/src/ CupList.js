import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";

const CupList = ({ cups_list }) => (
  <Transition
    items={cups_list}
    keys={cup => cup.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { cup => style => (
      <div style={style}>
        <Link to={"/cup/"+cup.id}>{cup.name}</Link>
      </div>
    )}
  </Transition>
);

export default CupList